package ru.lesson2.reflection.source_folder;

import ru.lesson2.reflection.RandomInt;

public class Phone {
    private String name;
    @RandomInt(min = 1, max = 12)
    private int coreNumber;

    public Phone(String name, int coreNumber){
        this.coreNumber = coreNumber;
        this.name = name;
    }

    public Phone(){};

    public String getName() {
        return name;
    }

    public int getCoreNumber() {
        return coreNumber;
    }
}