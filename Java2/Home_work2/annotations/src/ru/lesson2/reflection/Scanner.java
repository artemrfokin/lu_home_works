package ru.lesson2.reflection;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Scanner {
    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException {

        String inputFolder = "src\\ru\\lesson2\\reflection\\source_folder";
        Path dir = Paths.get(inputFolder);

        List<String> list = Files.walk(dir)
                .map(path -> path.toString())
                .filter(path -> path.endsWith(".java"))
                .map(fileName ->
                        fileName.replace("src\\", "")
                        .replace("src/", "")
                        .replace("\\", ".")
                        .replace("/", ".")
                        .replace(".java", ""))
                .collect(Collectors.toList());
        list.stream().forEach(System.out::println);

        // Неработающее кастование
        List<Object> objectsList = new ArrayList<>();
        for (String fileName : list){
            Class<?> clazz = Class.forName(fileName);
            Object ins = RandomIntProcessor.process(clazz);
            objectsList.add(ins);
        }
        objectsList.stream().forEach(System.out::println);

        // Работающее кастование
//        Class<?> clazz = Class.forName(list.get(0));
//        Phone ins2 = (Phone) clazz.newInstance();
    }
}
