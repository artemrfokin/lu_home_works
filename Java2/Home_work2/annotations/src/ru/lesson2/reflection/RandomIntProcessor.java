package ru.lesson2.reflection;

import ru.lesson2.reflection.source_folder.Phone;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Random;

public class RandomIntProcessor {
    public static <T> T process(Class<T> classOfT) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<?> constructor = Arrays.stream(classOfT.getDeclaredConstructors())
                .filter(cons -> cons.getParameterCount() == 0)
                .findFirst()
                .get();

        constructor.setAccessible(true);
        Object instance = constructor.newInstance();

        Field[] fields = classOfT.getDeclaredFields();
        setupFields(fields, instance);

        return (T) instance;
    }

    private static void setupFields(Field[] fields, Object object) throws IllegalAccessException {
        for (Field field : fields){
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            if (annotation != null){
                Random random = new Random();
                int randomInt = random.nextInt(annotation.max() - annotation.min() + 1) + annotation.min();
                field.setAccessible(true);
                field.set(object, randomInt);
            }
        }
    }
}
