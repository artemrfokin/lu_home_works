import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.locks.ReentrantLock;

public class Register {
    private TreeMap<Date, String> registerSet = new TreeMap<Date, String>();
    private ReentrantLock lock = new ReentrantLock();

    public void printLastRecords(int seconds){
        Date nowDate = new Date();
        long now = nowDate.getTime();
        long xMinutesAgo = now - (long)(seconds * 1000);

        System.out.println();
        System.out.println("Report time: " + nowDate);
        System.out.println("Time " + seconds + " seconds ago: " + new Date(xMinutesAgo));
        System.out.println();

        registerSet.entrySet()
                .stream()
                .filter(x -> {
                    long elementTime = x.getKey().getTime();
                    return  elementTime <= now & elementTime >= xMinutesAgo;})
                .forEach(x -> {
                    System.out.println(x.getValue() + ": " + x.getKey());
                });
    }


    public void addRecord(){
        lock.lock();
        try {
            registerSet.put(new Date(), Thread.currentThread().getName());
        } finally {
            lock.unlock();
        }
    }


    public void addRecord(Date time){
        lock.lock();
        try {
            registerSet.put(time, Thread.currentThread().getName());
        } finally {
            lock.unlock();
        }
    }
}
