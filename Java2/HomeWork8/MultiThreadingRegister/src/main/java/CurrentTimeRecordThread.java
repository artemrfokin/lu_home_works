import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class CurrentTimeRecordThread implements Runnable {
    private Register register;

    public CurrentTimeRecordThread(Register register) {
        this.register = register;
    }


    @Override
    public void run() {
        while (true){
            int randInt = ThreadLocalRandom.current().nextInt(0, 1000);
            try {
                Thread.sleep((long) randInt);
                register.addRecord();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
