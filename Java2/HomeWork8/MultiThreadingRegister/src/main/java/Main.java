import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {

        Register register = new Register();
//        CurrentTimeRecordThread threadType = new CurrentTimeRecordThread(register);
        ReceivedTimeRecordThread threadType = new ReceivedTimeRecordThread(register);
        ThreadsFactory threadsFactory = new ThreadsFactory(threadType);
        threadsFactory.createTheads();

        boolean active = true;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while(active){
                System.out.println("Please enter report period (in seconds). \nFor exit inter 'q'.");
                String line = reader.readLine();
                line = line.trim();
                if(!(line.equals("q"))){
                    int seconds = Integer.parseInt(line);
                    register.printLastRecords(seconds);
                    System.out.println();
                } else {
                    active = false;
                }
            }
        }
    }
}
