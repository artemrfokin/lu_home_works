import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class ReceivedTimeRecordThread implements Runnable{
    private Register register;

    public ReceivedTimeRecordThread(Register register) {
        this.register = register;
    }

    @Override
    public void run() {
        while (true){
            int randInt = ThreadLocalRandom.current().nextInt(0, 1000);
            try {
                Thread.sleep((long) randInt);

                long finishTime = new Date().getTime();
                long startTime = finishTime - 20 * 1000;
                long randLong = ThreadLocalRandom.current().nextLong(startTime, finishTime + 1);
                Date randDate = new Date(randLong);

                register.addRecord(randDate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
