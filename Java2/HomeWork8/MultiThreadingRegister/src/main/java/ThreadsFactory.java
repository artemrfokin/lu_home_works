import java.util.concurrent.ThreadLocalRandom;

public class ThreadsFactory {
    private Runnable threadType;

    public ThreadsFactory(Runnable threadType) throws InterruptedException {
        this.threadType = threadType;
    }


    public void createTheads() throws InterruptedException {
        for (int i = 0; i < 3; i++ ){
            Thread newThread = new Thread(threadType);

            newThread.setDaemon(true);

            int randInt = ThreadLocalRandom.current().nextInt(0, 500);
            Thread.sleep((long) randInt);

            newThread.start();
        }
    }
}

