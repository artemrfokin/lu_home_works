package service;

public interface ApplicationUserService {
    void addUser(String login, String password);

    void updateUserInfo(Integer id, String login, String password);

    void showAllUsers();

    void deleteUser(int id);

    void authorisation(String login, String password);
}
