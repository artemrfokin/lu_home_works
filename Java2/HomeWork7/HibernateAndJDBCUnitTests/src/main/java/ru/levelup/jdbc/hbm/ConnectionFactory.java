package ru.levelup.jdbc.hbm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    public static Connection connection;

    static {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Chat",
                    "postgres",
                    "root");
        } catch (SQLException e) {
            throw new RuntimeException("Connection wasn't established", e);
        }
    }

    public static Connection getConnection(){
        return connection;
    }
}
