import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.levelup.jdbc.DBHandler;
import ru.levelup.jdbc.hbm.ConnectionFactory;
import ru.levelup.jdbc.hbm.SessionFactoryInitializer;
import ru.levelup.jdbc.hbm.UserHandler;
import ru.levelup.jdbc.hbm.domain.ApplicationUser;
import service.ApplicationUserServiceImpl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException {

        /* //        Presentation of work this DB with Hibernate        // */
//        SessionFactory factory = SessionFactoryInitializer.getFactory();
//        ApplicationUserServiceImpl service = new ApplicationUserServiceImpl(factory);
//        service.addUser(null, "");
//        service.updateUserInfo(7, "bb", "bc");
//        service.showAllUsers();
//        service.deleteUser(7);
//        service.authorisation("aa", "bb");



        /* //        Presentation of work this DB with JDBC        // */
//        DBHandler.addUser("cc", "cc");
        Connection connection = ConnectionFactory.getConnection();
        DBHandler dbHandler = new DBHandler(connection);
        dbHandler.updateUserData(8, "cc", "cn");
    }
}
