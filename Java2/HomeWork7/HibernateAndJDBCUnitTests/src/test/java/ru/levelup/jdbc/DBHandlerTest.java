package ru.levelup.jdbc;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RunWith(MockitoJUnitRunner.class)
public class DBHandlerTest {
    @Mock
    private Connection connection;
    @InjectMocks
    private DBHandler dbHandler;
    private PreparedStatement preparedStatement;


    @Before
    public void setUp() throws SQLException{
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(connection.prepareStatement(Mockito.anyString())).thenReturn(preparedStatement);

        ResultSet resultSet = Mockito.mock(ResultSet.class);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }



    /*  //                 UpdateUser Tests                    //  */
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateUserData_idIsNegative_throwsException() throws SQLException {
        dbHandler.updateUserData(-5L, "cc", "ca");
    }


    @Test
    public void testUpdateUserData_PrepareStatementUsingConfirmation() throws SQLException{
        dbHandler.updateUserData(1, "value1", "value2");
        Mockito.verify(connection).prepareStatement(Mockito.anyString());
    }


    @Test
    public void testUpdate_PrepareStatementWithCorrectParameters() throws SQLException{
        Long id = 5L;
        String login = "value1";
        String password = "value2";
        dbHandler.updateUserData(id, login, password);
        Mockito.verify(preparedStatement).setString(
                Mockito.eq(1), Mockito.eq(login));
        Mockito.verify(preparedStatement).setString(
                Mockito.eq(2), Mockito.eq(password));
        Mockito.verify(preparedStatement).setLong(
                Mockito.eq(3), Mockito.eq(id));
        Mockito.verify(preparedStatement).executeUpdate();
    }


    @Test
    public void testUpdateUser_ConnectionCloseConfirmation() throws SQLException{
        Long id = 5L;
        String login = "value1";
        String password = "value2";
        dbHandler.updateUserData(id, login, password);
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }



    /* //               Authorisation Tests              // */
    @Test(expected = IllegalArgumentException.class)
    public void testAuthorisation_loginIsNull_throwsException() throws SQLException{
        String login = null;
        String password = "value2";
        dbHandler.authorisation(login, password);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testAuthorisation_PasswordIsNull_throwsException() throws SQLException{
        String login = "value1";
        String password = null;
        dbHandler.authorisation(login, password);
    }


    @Test
    public void testAuthorisation_CorrectParametersInQueryConfirmation() throws SQLException{
        String login = "value1";
        String password = "value2";
        dbHandler.authorisation(login, password);
        Mockito.verify(preparedStatement).setString(
                Mockito.eq(1), Mockito.eq(login));
        Mockito.verify(preparedStatement).setString(
                Mockito.eq(2), Mockito.eq(password));
        Mockito.verify(preparedStatement).executeQuery();
    }


    @Test
    public void testAuthorisation_ConnectionClosingConfirmation() throws SQLException{
        String login = "value1";
        String password = "value2";
        dbHandler.authorisation(login, password);
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }



    /* //            ShowExactUser Tests            // */
    @Test(expected = IllegalArgumentException.class)
    public void testShowExactUser_LoginIsNull_throwsException() throws SQLException{
        String login = null;
        dbHandler.showExactUser(login);
    }


    @Test
    public void testShowExactUser_CorrectParametersInQueryConfirmation() throws SQLException{
        String login = "value1";
        dbHandler.showExactUser(login);
        Mockito.verify(preparedStatement).setString(
                Mockito.eq(1), Mockito.eq(login));
        Mockito.verify(preparedStatement).executeQuery();
    }


    @Test
    public void testShowExactUser_ConnectionClosingConfirmation() throws SQLException{
        String login = "value1";
        dbHandler.showExactUser(login);
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }



    /* //               DeleteUser Tests                 // */
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteUser_idIsNegative_throwsException() throws SQLException {
        long id = -3;
        dbHandler.deleteUser(id);
    }


    @Test
    public void testDeleteUser_RightParametersConfirmation() throws SQLException {
        long id = 6L;
        dbHandler.deleteUser(id);
        Mockito.verify(preparedStatement).setLong(
                Mockito.eq(1), Mockito.eq(id));
        Mockito.verify(preparedStatement).executeUpdate();
    }


    @Test
    public void testDeleteUser_ConnectionClosingConfirmation() throws SQLException{
        long id = 6L;
        dbHandler.deleteUser(id);
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }


    /* //               AddUser Tests               // */
    @Test(expected = IllegalArgumentException.class)
    public void testAddUser_loginIsNull_throwsException() throws SQLException {
        String login = null;
        String password = "value2";
        dbHandler.addUser(login, password);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testAddUser_passwordIsNull_throwsException() throws SQLException {
        String login = "value1";
        String password = null;
        dbHandler.addUser(login, password);
    }


    @Test
    public void testAddUser_RightParametersConfirmation() throws SQLException {
        String login = "value1";
        String password = "value2";
        dbHandler.addUser(login, password);
        Mockito.verify(preparedStatement).setString(
                Mockito.eq(1), Mockito.eq(login));
        Mockito.verify(preparedStatement).setString(
                Mockito.eq(2), Mockito.eq(password));
        Mockito.verify(preparedStatement).executeUpdate();
    }


    @Test
    public void testUpdateUser_ConnectionClosingConfirmation() throws SQLException{
        String login = "value1";
        String password = "value2";
        dbHandler.addUser(login, password);
        Mockito.verify(preparedStatement).close();
        Mockito.verify(connection).close();
    }


    /* //            ShowAllUsers Tests - nothing to test        // */

}
