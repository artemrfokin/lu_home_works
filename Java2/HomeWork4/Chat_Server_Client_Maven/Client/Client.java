package org.level.up.client;

import org.level.up.client.message.Message;
import org.level.up.json.JsonService;
import org.level.up.json.impl.JsonServiceImpl;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket = new Socket(InetAddress.getByName("localhost"), 7878);
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedWriter toServer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        Thread listenThread = new Thread(new ClientLisenerThread(fromServer));
        listenThread.setDaemon(true);
        listenThread.start();

        String inputString = "";
        JsonService service = new JsonServiceImpl();
        System.out.println("Enter string");
        while (true) {
            inputString = consoleReader.readLine();
            Message message = new Message(inputString);
            String stringMessageAsJson = service.toJson(message);

            toServer.write(stringMessageAsJson + "\n");
            toServer.flush();
            if (inputString.equalsIgnoreCase("exit")) {
                break;
            }
        }
    }
}
