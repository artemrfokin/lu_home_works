package org.level.up.json.test;

import org.level.up.json.SerializedBy;

@SerializedBy(
        serializer = CatJsonSerialisator.class,
        deserializer = CatJsonDeserialisator.class
)
public class Cat {
    private String name;
    private int age;

    public Cat(){}; // надо всегда оставлять дефолтный конструктор чтобы рефлксией можно было создать обхекты и выгрузить все поля

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
