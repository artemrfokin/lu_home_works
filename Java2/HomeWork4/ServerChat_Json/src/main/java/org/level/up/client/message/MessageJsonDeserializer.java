package org.level.up.client.message;

import org.level.up.json.JsonDeserializer;
import org.level.up.json.test.Cat;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class MessageJsonDeserializer implements JsonDeserializer<Message> {

    @Override
    public Message deserialize(String json) {
        String withOutBrackets = json.replace("{", "").replace("}", "");
        String withOutQuotes = withOutBrackets.replace("\"", "");
        String[] split = withOutQuotes.split(",");

        Message message = new Message();
        Arrays.stream(split)
                .forEach(fieldValue -> {
                    String[] object = fieldValue.split(":", 2);
                    setupFiled(message, object);
                });

        return message;
    }

    private void setupFiled(Message message, String[] object){
        Class<?> messageClass= message.getClass();
        Field field = null;
        try {
            field = messageClass.getDeclaredField(object[0]);
            field.setAccessible(true);
            if (field.getType() == Date.class){
                SimpleDateFormat fromStringFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                Date parsedDate = fromStringFormat.parse(object[1]);
                field.set(message, parsedDate);
            } else {
                field.set(message, object[1]);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
