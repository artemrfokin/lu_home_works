package org.level.up.json.test;

public class FromClassTest {
    public static void main(String[] args) throws ClassNotFoundException {
        String reducePart = "src\\main\\java\\";

        String path = "src\\main\\java\\org\\level\\up\\client\\Client.java";

        String finalStr = path.replace(reducePart, "")
                .replace("\\", ".")
                .replace("/", ".")
                .replace(".java", "");

        System.out.println(finalStr);



//        Class<?> aClass = Class.forName("org.level.up.client.Client");
//        System.out.println(aClass);
    }
}
