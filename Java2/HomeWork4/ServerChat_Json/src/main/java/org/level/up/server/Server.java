package org.level.up.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Server {
    private static int clients = 0;
    private static long currentId;
    static Map<Long, ClientWorker> clientsMap = new HashMap<>();

    private Server() {}

    public static void minusClent() {
        clients--;
    }
    public static void plusClient(){
        clients++;
    }
    public static int getClients(){
        return clients;
    }

    public static long getCurrentId() {
        return currentId;
    }
    public static void incrementCurrentId() {
        currentId++;
    }

    public static void startServer(int port) {
        try {
            ServerSocket server = new ServerSocket(port);
            System.out.println("Server has started at port " + port);

            while (true) {
                Socket client = server.accept();
                plusClient();
                incrementCurrentId();

                ClientWorker clientWorker = new ClientWorker(client, currentId);
                Thread clientThread = new Thread(clientWorker);
                clientsMap.put(currentId, clientWorker);
                clientThread.start();

            }
        }
        catch(
    IOException exc)

    {
        exc.printStackTrace();
    }
}
}
