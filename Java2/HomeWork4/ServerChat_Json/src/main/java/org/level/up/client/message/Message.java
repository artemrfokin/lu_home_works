package org.level.up.client.message;

import org.level.up.json.SerializedBy;

import java.text.SimpleDateFormat;
import java.util.Date;

@SerializedBy(
        serializer = MessageJsonSerializer.class,
        deserializer = MessageJsonDeserializer.class
)
public class Message {
    String id;
    String text;
    Date sendingTime;

    public Message(){};

    public Message(String text) {
        this.id = "";
        this.text = text;
//        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
//        this.sendingTime = formatter.format(new Date());
        this.sendingTime = new Date();
    }

    public String getText() {
        return text;
    }

    public Date getSendingTime() {
        return sendingTime;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
