package org.level.up.client;

import org.level.up.client.message.Message;
import org.level.up.json.JsonService;
import org.level.up.json.impl.JsonServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;

// This thread reads input stream of client and prints it in concole.
public class ClientLisenerThread implements Runnable{
    private BufferedReader inputStream;
    public ClientLisenerThread(BufferedReader in){
        inputStream = in;
    }

    @Override
    public void run() {
        JsonService service = new JsonServiceImpl();
        do {
            String jsonFromServer = null;
            try {
                jsonFromServer = inputStream.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Message msgFromServer = service.fromJson(jsonFromServer, Message.class);

            System.out.println("Client id = " + msgFromServer.getId());
            System.out.println(msgFromServer.getText());
            System.out.println(msgFromServer.getSendingTime());
            System.out.println();
        }
        while (true);
    }
}
