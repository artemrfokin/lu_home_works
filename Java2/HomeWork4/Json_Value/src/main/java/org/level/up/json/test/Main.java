package org.level.up.json.test;

import org.level.up.json.JsonService;
import org.level.up.json.impl.JsonServiceImpl;

public class Main {
    public static void main(String[] args) {
        JsonService service = new JsonServiceImpl();
        Cat cat = new Cat("Mursik", 5);
        String json = service.toJson(cat);
        System.out.println(json);

        Cat fromJson = service.fromJson(json, Cat.class);

        System.out.println();
        System.out.println(fromJson.getName());
        System.out.println(fromJson.getAge());
    }
}
