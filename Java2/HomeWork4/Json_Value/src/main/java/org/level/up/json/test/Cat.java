package org.level.up.json.test;

import org.level.up.json.JsonValue;
import org.level.up.json.SerializedBy;

@SerializedBy(
        serializer = CatJsonPartlySerializer.class,
        deserializer = CatJsonPartlyDeserializer.class
)
public class Cat {
    @JsonValue(fieldName = "NewName")
    private String name;
    @JsonValue(fieldName = "")
    private int age;

    public Cat(){}; // надо всегда оставлять дефолтный конструктор чтобы рефлксией можно было создать обхекты и выгрузить все поля

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
