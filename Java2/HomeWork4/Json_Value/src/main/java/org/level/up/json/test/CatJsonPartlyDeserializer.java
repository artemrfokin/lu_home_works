package org.level.up.json.test;

import org.level.up.json.JsonDeserializer;

import java.lang.reflect.Field;
import java.util.Arrays;

public class CatJsonPartlyDeserializer implements JsonDeserializer<Cat> {
    @Override
    public Cat deserialize(String json) {
        String withOutBrackets = json.replace("{", "").replace("}", "");
        String withOutQuotes = withOutBrackets.replace("\"", "");
        String[] split = withOutQuotes.split(",");

        Cat cat = new Cat();
        Arrays.stream(split)
                .forEach(fieldValue -> {
                    String[] object = fieldValue.split(":");
//                    System.out.println("object" + object.toString());
                    setupFiled(cat, object);
                });
        return cat;
    }

    private void setupFiled(Cat cat, String[] object){
        Class<?> catClass = cat.getClass();
//        Field field = null;
        try {
            Field[] fieldArray = catClass.getDeclaredFields();
//            System.out.println(fieldArray.toString());
            for (Field field : fieldArray){
//                System.out.println(object[0]);
                if (field.getName().equals(object[0])){
                    field.setAccessible(true);
                    if (field.getType() == int.class){
                        setupInrField(cat, field, object[1]);
                    } else {
                        field.set(cat, object[1]);
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void setupInrField(Cat cat, Field field, String value) throws IllegalAccessException {
        field.set(cat, Integer.parseInt(value));
    }
}
