package org.level.up.json.test;

import org.level.up.json.JsonSerializer;
import org.level.up.json.JsonValue;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CatJsonPartlySerializer implements JsonSerializer<Cat> {

    @Override
    public String serialize(Cat object) {
        String json = "{";
        Class<?> clazz = object.getClass();
        List<Field> fieldsList = Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> {
                    Annotation[] annotations = field.getAnnotations();
                    return Arrays.stream(annotations)
                            .anyMatch(a -> a.annotationType() == JsonValue.class);
                })
                .collect(Collectors.toList());

        for (int i = 0; i < fieldsList.size(); i++){
            Field field = fieldsList.get(i);
            field.setAccessible(true);

            json += "\"";
            JsonValue annotation = field.getAnnotation(JsonValue.class);
            if (annotation != null){
                if (!annotation.fieldName().equals("")){
                    json += annotation.fieldName();
                } else {
                    json += field.getName();
                }
            }
            json += "\"";
            json += ":";
            if (field.getType() == String.class){
                json += "\"";
                try {
                    json += field.get(object);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                json += "\"";
            } else {
                Class<?> fieldClass = field.getType();
                if (fieldClass.isInstance(Number.class) || fieldClass == int.class){
                    try {
                        json += field.get(object);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (i != fieldsList.size() - 1){
                json += ",";
            } else {
                json += "}";
            }
        }
        return json;
    }
}
