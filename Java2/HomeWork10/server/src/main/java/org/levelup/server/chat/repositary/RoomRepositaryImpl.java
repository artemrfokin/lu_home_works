package org.levelup.server.chat.repositary;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.levelup.server.chat.domain.Room;
import org.levelup.server.chat.database.SessionFactoryinitioalizer;
import org.postgresql.util.PSQLException;

import java.util.Collection;
import java.util.List;

public class RoomRepositaryImpl implements RoomRepository{
    private SessionFactory factory;

    public RoomRepositaryImpl() {
        this.factory = SessionFactoryinitioalizer.getFactory();
    }

    @Override
    public Room createRoom(String name) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Room room = new Room(name);
        session.save(room);

        transaction.commit();
        session.close();

        return room;
    }


    @Override
    public Collection<Room> findAll() {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Room", Room.class);
        List<Room> list = query.list();

        transaction.commit();
        session.close();

        return list;
    }
}
