package org.levelup.server.chat.command;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class CommandParser {
    private Map<String, CommandExecutor> executors;

    {
        executors = new HashMap<>();
        executors.put("rooms showAll", new GetRoomsCommandExecutor());
        executors.put("rooms create", new CreateRoomCommandExecutor());
    }


    public void executeCommand(String line) throws IOException {
        CommandExecutor executor = executors.get(line);
        if (executor != null){
            executor.execute();
        } else {
            System.out.println("Entered wrong command");
        }
    }
}
