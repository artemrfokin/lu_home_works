package org.levelup.server.chat;

import org.levelup.server.chat.command.CommandParser;
import org.levelup.server.chat.domain.Room;
import org.levelup.server.chat.repositary.RoomRepositaryImpl;

import java.io.*;
import java.net.Socket;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class ServerWorker implements Runnable{
    private Socket client;
    private long id;
    private BufferedReader in;
    private BufferedWriter out;


    public ServerWorker(Socket client, long id) throws IOException {
        this.client = client;
        this.id = id;
        in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
    }

    public long getId() {
        return id;
    }

    @Override
    public void run(){
        try {
            System.out.println("Client # " + ClientListener.getCurrentId() + " connected." + "\n"
                    + "Total quantity of clients: " +  ClientListener.getClients());
            System.out.println();

            String clientMessage = "";
            while (true){
                clientMessage = in.readLine();
                if (!clientMessage.equals("exit")){
                    if (clientMessage.equals("rooms showAll")){
                        RoomRepositaryImpl roomRepositary = new RoomRepositaryImpl();
                        Collection<Room> rooms = roomRepositary.findAll();
                        String roomsString = rooms.stream().
                                map(x -> x.getName()).
                                collect(Collectors.toList())
                                .toString();

                        send("List of rooms: " + roomsString);
                    } else {
                        for (Map.Entry entry : ClientListener.clientsMap.entrySet()){
                            ServerWorker cw = (ServerWorker) entry.getValue();
                            cw.send(clientMessage);
                        }
                    }
                } else {
                    ClientListener.clientsMap.remove(getId());
                    break;
                }
            }
            System.out.println("Worker # " + id + " has closed");
            ClientListener.minusClent();
            System.out.println("Number of clients " + ClientListener.getClients());
            System.out.println();

        } catch (IOException exc){
            exc.printStackTrace();
        }
    }

    public void send(String message) throws IOException {
        out.write(message + "\n");
        out.flush();
    }

}
