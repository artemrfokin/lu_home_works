package org.levelup.server.chat;

import org.levelup.server.chat.command.CommandParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandWorker implements Runnable {
    private ChatServer chatServer;
    private final BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

    public CommandWorker(ChatServer chatServer) {
        this.chatServer = chatServer;
    }

    @Override
    public void run() {
        try {
            String line = "";
            CommandParser commandParser = new CommandParser();
            while (true){
                System.out.println("Plese enter command. To close server enter 'stop'.");
                line = console.readLine();
                if (!"stop".equalsIgnoreCase(line)){
                    commandParser.executeCommand(line);
                } else break;
            }

            chatServer.stopServer();
            console.close();
        } catch (IOException exc){
            exc.printStackTrace();
        }
    }
}
