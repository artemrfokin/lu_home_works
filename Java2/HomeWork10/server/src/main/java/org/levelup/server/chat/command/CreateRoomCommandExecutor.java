package org.levelup.server.chat.command;

import org.levelup.server.chat.domain.Room;
import org.levelup.server.chat.repositary.RoomRepositaryImpl;
import org.levelup.server.chat.repositary.RoomRepository;
import org.postgresql.util.PSQLException;

import javax.persistence.PersistenceException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CreateRoomCommandExecutor implements CommandExecutor {
    private final RoomRepository roomRepository;

    public CreateRoomCommandExecutor(){
        roomRepository = new RoomRepositaryImpl();
    }

    @Override
    public void execute() throws IOException {
        Room room = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String line = "";
            while (!line.equalsIgnoreCase("cancel")){
                System.out.println("Please enter name of new room: ");
                line = reader.readLine();
                if (!line.equals("")){
                    try {
                        room = roomRepository.createRoom(line);
                        break;
                    } catch (PersistenceException exc){
                        System.out.println("Room with such name already exists.");
                    }
                } else {
                    System.out.println("Room's name can't be empty.");
                }
            }


        if(room != null){
            System.out.printf("Room with name %s has been created.\n", room.getName());
        } else {
            System.out.println("Room was not created. Please try again.");
        }

    }
}
