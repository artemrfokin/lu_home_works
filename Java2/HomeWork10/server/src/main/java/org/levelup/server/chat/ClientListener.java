package org.levelup.server.chat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ClientListener implements Runnable {
    private final ServerSocket server;
    private volatile boolean running = true;
    private ExecutorService executorService = Executors.newFixedThreadPool(10);
    private static int clients = 0;
    private static long currentId;
    static Map<Long, ServerWorker> clientsMap = new HashMap<>();

    public ClientListener(ServerSocket server) {
        this.server = server;
    }

    public static void minusClent() {
        clients--;
    }
    public static void plusClient() {
        clients++;
    }
    public static int getClients() {
        return clients;
    }


    public static long getCurrentId() {
        return currentId;
    }
    public static void incrementCurrentId() {
        currentId++;
    }


    @Override
    public void run() {
        while (running){
            try {
                Socket client = server.accept();
                plusClient();
                incrementCurrentId();

                ServerWorker serverWorker = new ServerWorker(client, currentId);
                clientsMap.put(currentId, serverWorker);
                executorService.submit(serverWorker);
            }
            catch (IOException ignored){
            }
        }
    }


    public void shutDown() {
        try{
            running = false;
            server.close();
            executorService.shutdown();
        }
        catch (IOException exc){
            System.out.println("Server stopped..");
        }
    }
}
