package org.levelup.server.chat.command;

import java.io.IOException;

public interface CommandExecutor {
    public void execute() throws IOException;
}
