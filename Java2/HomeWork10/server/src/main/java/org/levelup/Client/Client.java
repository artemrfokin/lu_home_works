package org.levelup.Client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket = new Socket(InetAddress.getByName("localhost"), 1234);
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        Thread listenThread = new Thread(new ClientLisenerThread(in));
        listenThread.setDaemon(true);
        listenThread.start();

        String message = "";
        System.out.println("Enter string");
        while (true) {
            message = consoleReader.readLine();
            out.write(message + "\n");
            out.flush();
            if (message.equalsIgnoreCase("exit")) {
                break;
            }
        }
    }
}
