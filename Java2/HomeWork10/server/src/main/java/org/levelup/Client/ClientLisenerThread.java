package org.levelup.Client;

import java.io.BufferedReader;
import java.io.IOException;

// This thread reads input stream of client and prints it in concole.

public class ClientLisenerThread implements Runnable{
    private BufferedReader inputStream;
    public ClientLisenerThread(BufferedReader in){
        inputStream = in;
    }

    @Override
    public void run() {
        do {
            String inMessage = null;
            try {
                inMessage = inputStream.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("New message " + inMessage);
            System.out.println();
        }
        while (true);
    }
}
