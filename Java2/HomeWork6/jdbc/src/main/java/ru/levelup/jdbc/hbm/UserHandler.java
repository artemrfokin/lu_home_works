package ru.levelup.jdbc.hbm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.levelup.jdbc.hbm.domain.ApplicationUser;

import java.util.List;


public class UserHandler {
    private static SessionFactory factory;
    private static Session session;
    private static Transaction transaction;


    private static void openConnection() {
        factory = SessionFactoryInitializer.getFactory();
        session = factory.openSession();
        transaction = session.beginTransaction();
    }

    private static void closeConnection() {
        transaction.commit();
        session.close();
        factory.close();
    }

    // Addintion Users
    public static void addUser(String login, String password) {
        openConnection();

        ApplicationUser user = new ApplicationUser();
        user.setUserLogin(login);
        user.setPassword(password);
        session.persist(user);

        closeConnection();
    }


    // To update user's info
    public static void updateUserInfo(int id, String login, String password) {
        openConnection();

        ApplicationUser user = session.get(ApplicationUser.class, id);
        user.setUserLogin(login);
        user.setPassword(password);
        session.persist(user);

        closeConnection();
    }


    // To showAll
    public static void showAllUsers() {
        openConnection();

        Query query = session.createQuery("from ApplicationUser");
        List<ApplicationUser> result = query.list();
        for (ApplicationUser object : result) {
            System.out.printf("Login: %s, password: %s \n", object.getUserLogin(), object.getPassword());
        }

        closeConnection();
    }


    // Delete user
    public static void deleteUser(int id) {
        openConnection();

        Query query = session.createQuery("delete ApplicationUser where id = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        closeConnection();
    }


    // Authorisation
    public static void authorisation(String login, String password) {
        openConnection();

        Query query = session.createQuery("from ApplicationUser where userLogin= :login and password = :password");
        query.setParameter("login", login);
        query.setParameter("password", password);
        List<ApplicationUser> result = query.list();
        if (result.size() != 0){
            ApplicationUser user = result.get(0);
                System.out.printf("Hello %s! You are authorised.\n", user.getUserLogin());
        } else {
            System.out.printf("You entered wrong login or password.\n");
        }

        closeConnection();
    }
}
