package ru.levelup.jdbc.hbm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.levelup.jdbc.hbm.domain.ApplicationUser;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        UserHandler.addUser("Babai", "Zhiza");
//        UserHandler.updateUserInfo(5, "Lokki", "Chpokki");
//        UserHandler.showAllUsers();
//        UserHandler.deleteUser(5);
        UserHandler.authorisation("Petrovich", "pps");
    }
}
