package org.level.up.json.impl;

import org.level.up.json.JsonDeserializer;
import org.level.up.json.JsonSerializer;
import org.level.up.json.JsonService;
import org.level.up.json.SerializedBy;

public class JsonServiceImpl implements JsonService {

    @Override
    public String toJson(Object object) {
        Class<?> clazz = object.getClass();
        SerializedBy annotation = clazz.getAnnotation(SerializedBy.class);
        if (annotation == null){
            throw new RuntimeException("");
        }
        Class<? extends JsonSerializer> serializer = annotation.serializer();
        JsonSerializer jsonSerializer = JsonStorage.serializers.entrySet()
                .stream().filter(entry -> entry.getKey().equalsIgnoreCase(serializer.getSimpleName()))
                .findFirst()
                .get()
                .getValue();
        return jsonSerializer.serialize(object);
    }

    @Override
    public <T> T fromJson(String json, Class<T> classOF) {
        SerializedBy annotation = classOF.getAnnotation(SerializedBy.class);
        if (annotation == null){
            throw new RuntimeException("");
        }
        Class<? extends JsonDeserializer> deserializer = annotation.deserializer();
        JsonDeserializer jsonDeserializer = JsonStorage.deserializers.entrySet()
                .stream().filter(entry -> entry.getKey().equalsIgnoreCase(deserializer.getSimpleName()))
                .findFirst()
                .get()
                .getValue();

        return (T) jsonDeserializer.deserialize(json);
    }
}
