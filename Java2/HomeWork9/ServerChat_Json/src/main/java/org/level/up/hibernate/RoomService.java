package org.level.up.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.level.up.hibernate.domain.Room;
import org.level.up.hibernate.domain.User;

import java.util.ArrayList;
import java.util.List;

public class RoomService {
    private SessionFactory factory;

    public RoomService(SessionFactory factory) {
        this.factory = factory;
    }


    public void showAllRooms(){
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Room");
        List<Room> result = query.list();
        for (Room room : result){
            System.out.printf("Room name - %s, Active - %b; \n", room.getName(), room.isActive());
        }

        transaction.commit();
        session.close();
    }

    public void addUserToRoom(long roomId, int idUser1){
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Room room = session.get(Room.class, roomId);
        User user = session.get(User.class, idUser1);
        user.getRooms().add(room);
        session.persist(user);

        transaction.commit();
        session.close();
    }

    public void addRoom(String name, boolean isActive){
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Room room = new Room();
        room.setName(name);
        room.setActive(isActive);
        session.persist(room);

        transaction.commit();
        session.close();

    }

    public void deleteRoom(long id){
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("delete Room where id = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        transaction.commit();
        session.close();

    }


}
