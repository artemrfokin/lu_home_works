package org.level.up.server;

import org.hibernate.SessionFactory;
import org.level.up.hibernate.RoomService;
import org.level.up.hibernate.SessionFactoryInitializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ServerApplication {
    public static void main(String[] args) throws InterruptedException, IOException {

        Server server = new Server(7878);
        Thread serverSocketThread = new Thread(server);
        serverSocketThread.start();

        SessionFactory factory = SessionFactoryInitializer.getFactory();
        RoomService roomService = new RoomService(factory);


        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            boolean active = true;
            while (active) {
                System.out.println("Please enter command.\nWrite 'close' to stop server. \n");
                String line = reader.readLine();
                if (line.equals("close")) {
                    serverSocketThread.interrupt();
                    server.closeSocket();
                    active = false;
                } else if (line.equals("showAll;")) {
                    roomService.showAllRooms();
                    System.out.println();
                } else if (line.startsWith("addRoom ")) {
                    String[] commandParts = line.split(" ");
                    roomService.addRoom(commandParts[1], Boolean.parseBoolean(commandParts[2]));
                    System.out.println();
                } else if (line.startsWith("deleteRoom ")) {
                    String[] commandParts = line.split(" ");
                    roomService.deleteRoom(Long.parseLong(commandParts[1]));
                    System.out.println();
                }
            }
        }
    }
}
