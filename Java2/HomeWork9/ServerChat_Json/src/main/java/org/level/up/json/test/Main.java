package org.level.up.json.test;

import org.level.up.client.message.Message;
import org.level.up.json.JsonService;
import org.level.up.json.impl.JsonServiceImpl;

public class Main {
    public static void main(String[] args) {
        JsonService service = new JsonServiceImpl();
//        Cat cat = new Cat("Mursik", 5);
//        String json = service.toJson(cat);

//        Cat fromJson = service.fromJson(json, Cat.class);
//        System.out.println();
//        System.out.println(fromJson.getAge());
//        System.out.println(fromJson.getName());

        Message message = new Message("Hello");
        String json = service.toJson(message);
        System.out.println(json);


        Message fromJson = service.fromJson(json, Message.class);
        System.out.println();
        System.out.println(fromJson.getText());
        System.out.println(fromJson.getSendingTime());
    }
}
