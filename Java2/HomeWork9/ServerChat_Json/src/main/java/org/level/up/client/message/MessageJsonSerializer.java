package org.level.up.client.message;

import org.level.up.json.JsonSerializer;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageJsonSerializer implements JsonSerializer<Message> {
    @Override
    public String serialize(Message object) {
        Date date = object.getSendingTime();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String stringDate = format.format(date);
        return "{"
                + "\"" + "id" + "\"" + ":" + "\"" + object.getId() + "\""
                + ","
                + "\"" + "text" + "\"" + ":" + "\"" + object.getText() + "\""
                + ","
                + "\"" + "sendingTime" + "\"" + ":" + "\"" + stringDate + "\""
                + "}";
    }
}

