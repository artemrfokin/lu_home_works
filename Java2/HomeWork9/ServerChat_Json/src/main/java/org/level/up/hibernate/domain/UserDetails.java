package org.level.up.hibernate.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "user_info")
public class UserDetails implements Serializable {

    @Id
    @OneToOne
    private User user;

    private String name;
    private Timestamp last_login;
    private String email;
    private String phone;
}

