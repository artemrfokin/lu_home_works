package org.level.up.hibernate.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "room")
//@Setter
//@Getter
public class Room {

    @Id
    @GeneratedValue
    private long id;
    private String name;
    private boolean active;

    @ManyToMany(mappedBy = "rooms")
    private Collection<User> usersInRoom;

    @OneToMany(mappedBy = "room")
    private Collection<Message> messages;


    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Collection<User> getUsersInRoom() {
        return usersInRoom;
    }

    public void setUsersInRoom(Collection<User> usersInRoom) {
        this.usersInRoom = usersInRoom;
    }

    public Collection<Message> getMessages() {
        return messages;
    }

    public void setMessages(Collection<Message> messages) {
        this.messages = messages;
    }
}