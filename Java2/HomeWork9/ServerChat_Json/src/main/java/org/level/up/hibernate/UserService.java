package org.level.up.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.level.up.hibernate.domain.User;

import java.util.List;

public class UserService {
    private SessionFactory factory;

    public UserService(SessionFactory factory) {
        this.factory = factory;
    }

//    public void addUser(String login, String password){
//        Session session = factory.openSession();
//        Transaction transaction = session.beginTransaction();
//
//        User user = new User();
//        user.set
//    }

    public void showAllUsers(){
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from User");
        List<User> list = query.list();
        for (User user : list){
            System.out.printf("User login: %s, user password %s \n", user.getLogin(), user.getPassword());
        }

        transaction.commit();
        session.close();
    }
}
