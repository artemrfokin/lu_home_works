package org.level.up.server;

import org.level.up.client.message.Message;
import org.level.up.json.JsonService;
import org.level.up.json.impl.JsonServiceImpl;

import java.io.*;
import java.net.Socket;
import java.util.Map;

public class ClientWorker implements Runnable {
    private Socket client;
    private long id;
    private BufferedReader in;
    private BufferedWriter out;


    public ClientWorker(Socket client, long id) throws IOException{
        this.client = client;
        this.id = id;
        in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
    }

    public long getId() {
        return id;
    }

    @Override
    public void run(){
        try {
            System.out.println("Client # " + Server.getCurrentId() + " connected." + "\n"
                    + "Total quantity of clients: " +  Server.getClients());
            System.out.println();

            // Message structuring
            String clientMessage = "";
            JsonService service = new JsonServiceImpl();

            while (true){
                clientMessage = in.readLine();
                Message msg = service.fromJson(clientMessage, Message.class);
                if (!msg.getText().equals("exit")){
                    msg.setId(String.valueOf(id));
                    clientMessage = service.toJson(msg);

                    for (Map.Entry entry : Server.clientsMap.entrySet()){
                        ClientWorker cw = (ClientWorker) entry.getValue();
                        cw.send(clientMessage);
                    }
                } else {
                    Server.clientsMap.remove(getId());
                    break;
                }
            }
            System.out.println("Worker # " + id + " has closed");
            Server.minusClent();
            System.out.println("Number of clients " + Server.getClients());
            System.out.println();

        } catch (IOException exc){
            exc.printStackTrace();
        }
    }

    public void send(String json) throws IOException {
        out.write(json + "\n");
        out.flush();
    }
}
