package org.level.up.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Server implements Runnable {
    private int port;
    private static int clients = 0;
    private static long currentId;
    ServerSocket serverSocket;
    static Map<Long, ClientWorker> clientsMap = new HashMap<>();
    private ExecutorService executorService = Executors.newFixedThreadPool(10);

    public Server(int port) {
        this.port = port;
    }


    public static void minusClent() {
        clients--;
    }

    public static void plusClient() {
        clients++;
    }

    public static int getClients() {
        return clients;
    }


    public static long getCurrentId() {
        return currentId;
    }

    public static void incrementCurrentId() {
        currentId++;
    }


    public void closeSocket() throws IOException {
        serverSocket.close();
    }


    public void startServer() {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server has started at port " + port);

            Thread currentThread = Thread.currentThread();

            while (!currentThread.isInterrupted()) {
                try {
                    Socket client = serverSocket.accept();
                    plusClient();
                    incrementCurrentId();

                    ClientWorker clientWorker = new ClientWorker(client, currentId);
                    clientsMap.put(currentId, clientWorker);

                    executorService.submit(clientWorker);
                } catch (SocketException e) {
                    continue;
                }
            }

            executorService.shutdown();

        } catch (IOException exc) {
            exc.printStackTrace();}
    }


    @Override
    public void run() {
        startServer();
    }
}
