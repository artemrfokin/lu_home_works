package arrayutil;
import java.util.Arrays;

public class ArrayUtil {
    public static int min(int[] array){
        Arrays.sort(array);
        return array[0];
    }

    public static int max(int[] array){
        Arrays.sort(array);
        return array[array.length - 1];
    }
}
