package arrayutil;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        int[] array1 = {2, 84, 32, 56468};
        int[] array2 = {894, 16, 984, 849};

        System.out.printf("Исходный массив: %s\n", Arrays.toString(array1));
        System.out.printf("Минимальное число = %d\n", ArrayUtil.min(array1));
        System.out.println("");

        System.out.printf("Исходный массив: %s\n", Arrays.toString(array2));
        System.out.printf("Максимальное число = %d\n", ArrayUtil.max(array2));



    }
}
