package carlist;

public class App {
    public static void main(String[] args) {
        // Использование конструктора без параметров
        Car c1 = new Car();
        c1.manufacturer = "Toyta";
        c1.model = "HighLander";
        c1.year = "2019";

        Car c2 = new Car();
        c2.manufacturer = "Toyota";
        c2.model = "Rav4";
        c2.year = "2016";

        // Использование конструктора с параметрами
        Car c3 = new Car("Toyota", "Camry", "2015");
        Car c4 = new Car("Toyota", "LandCruser", "2019");

        Car[] carList = new Car[4];
        carList[0] = c1;
        carList[1] = c2;
        carList[2] = c3;
        carList[3] = c4;

        for (Car c : carList){
            System.out.printf("Manufacturer: %s, model: %s, year: %s\n", c.manufacturer, c.model, c.year);
        }
    }
}
