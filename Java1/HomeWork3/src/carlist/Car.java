package carlist;


public class Car {
    String manufacturer;
    String model;
    String year;

    Car(){
    }

    Car(String manufacturer, String model, String year){
        this.manufacturer = manufacturer;
        this.model = model;
        this.year = year;
    }
}
