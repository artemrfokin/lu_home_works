package calculator;

public class App {
    public static void main(String[] args) {
        Calculator c = new Calculator();

        // Суммирование
        System.out.printf("Сумма целых чисел = %d\n", c.sum(4, 7));
        System.out.printf("Сумма чисел с плавающей точкой= %.2f\n", c.sum(4.0d, 7.0d));
        System.out.printf("Сумма чисел типа long = %,d\n", c.sum(2150000000L, 2148000000L));
        System.out.println("");

        // Вычитание
        System.out.printf("Разность целых чисел = %d\n", c.subtract(4, 7));
        System.out.printf("Разность чисел с плавающей точкой= %.2f\n", c.subtract(4.0d, 7.0d));
        System.out.printf("Разность чисел типа long = %,d\n", c.subtract(2150000000L, 2148000000L));
        System.out.println("");

        // Умножение
        System.out.printf("Произведение целых чисел = %d\n", c.multiply(4, 7));
        System.out.printf("Произведение чисел с плавающей точкой= %.2f\n", c.multiply(4.0d, 7.0d));
        System.out.printf("Произведение чисел типа long = %,d\n", c.multiply(2150000000L, 2148000000L));
        System.out.println("");

        // Деление
        System.out.printf("Частное целых чисел = %d\n", c.divide(4, 7));
        System.out.printf("Частное чисел с плавающей точкой= %.2f\n", c.divide(4.0d, 7.0d));
        System.out.printf("Частное чисел типа long = %,d\n", c.divide(2150000000L, 2148000000L));
        System.out.println("");

    }
}
