package calculator;

public class Calculator {
    // Суммирование
    public int sum(int a, int b){
        return a + b;
    }

    public double sum(double a, double b){
        return a + b;
    }

    public long sum(long a, long b){
        return a + b;
    }

    // Вычитание
    public int subtract(int a, int b){
        return a - b;
    }

    public double subtract(double a, double b){
        return a - b;
    }

    public long subtract(long a, long b){
        return a - b;
    }

    // Умножение
    public int multiply(int a, int b){
        return a * b;
    }

    public double multiply(double a, double b){
        return a * b;
    }

    public long multiply(long a, long b){
        return a * b;
    }

    // Деление
    public int divide(int a, int b){
        return a / b;
    }

    public double divide(double a, double b){
        return a / b;
    }

    public long divide(long a, long b){
        return a / b;
    }
}
