package Structure;

import Structure.exceptions.EmptyQueueException;
import Structure.exceptions.QueueOverFlowException;

public class Queue extends AbstractQueue {
    private int size;
    private int sizeLimit = 5;
    private List.Element head;


    @Override
    public void add(Object value) {
        if (size == sizeLimit){
            throw new QueueOverFlowException();
        }

        List.Element el = new List().new Element(value);
        if (head == null) {
            head = el;
            size++;
        } else {
            List.Element current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(el);
            size++;
        }
    }

    @Override
    public void remove() {
        if (size > 0){
            head = head.getNext();
            size--;
        } else {
            throw new EmptyQueueException();
        }
    }

    @Override
    public Object poll() {
        return head.getValue();
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public String toString() {
        List.Element current = head;
        String result = "";
        for (int i = 0; i < size; i++) {
            result += current.getValue().toString() + ", ";
            current = current.getNext();
        }
        return result;
    }
}
