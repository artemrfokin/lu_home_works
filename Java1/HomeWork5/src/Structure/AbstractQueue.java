package Structure;

public abstract class AbstractQueue<T>{

    public abstract void add(T value);

    public abstract void remove();

    public abstract T poll();

    public abstract boolean isEmpty();

    public abstract int size();
}
