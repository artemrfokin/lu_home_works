package Structure;

import Structure.exceptions.EmptyStackException;
import Structure.exceptions.StackOverFlowException;

import java.util.Arrays;

public class StaticStack {
    private Object[] array;
    private int size;

    public StaticStack(){
        array = new Object[5];
    };

    public StaticStack(int initialCapacity){
        if (initialCapacity <= 0) {
            initialCapacity = 5;
        }
        array = new Object[initialCapacity];
    }

    public void push(Object value) throws StackOverFlowException{
        add(value);
    }

    public Object pop() throws EmptyStackException{
        if (size == 0){
            throw new EmptyStackException();
        }
        Object value = array[size - 1];
        size--;
        return value;
    }

    public Object peek(){
        return array[size - 1];
    }


    private void add(Object value) throws StackOverFlowException{
        if (array.length == size) {
            throw new StackOverFlowException();
        }
        array[size++] = value;
    }

    public int size(){
        return size;
    }

    @Override
    public String toString() {
        return "Stack: " + Arrays.toString(array);
    }
}
