package Structure;

public abstract class AbstractStructure<T> implements Structure, Iterable {

    public abstract T get(int index);

    public abstract void set(int index, T value);

    public abstract boolean isEmpty();

    public abstract int size();


}
