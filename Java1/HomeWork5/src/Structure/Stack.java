package Structure;

import java.util.Arrays;

public class Stack {
    private Object[] array;
    private int size;

    public Stack(){
        array = new Object[6];
    };

    public Stack(int initialCapacity){
        if (initialCapacity <= 0) {
            initialCapacity = 10;
        }
        array = new Object[initialCapacity];
    }

    public void push(Object value){
        add(value);
    }

    public Object pop(){
        Object value = array[size - 1];
        size--;
        return value;
    }

    public Object peek(){
        return array[size - 1];
    }


    private void add(Object value) {
        if (array.length == size) {
            Object[] oldArray = array;
            array = new Object[(int) (size * 1.5d)];
            System.arraycopy(oldArray, 0, array, 0, size); // и с динамическими сработает.
        }
        array[size++] = value;
    }

    public int size(){
        return size;
    }

    @Override
    public String toString() {
        return "Stack: " + Arrays.toString(array);
    }
}
