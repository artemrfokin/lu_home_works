package Structure;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

// Список на основе массива
public class DynamicArray extends AbstractStructure implements Structure, Iterable {

    private Object[] array;
    private int size; // количество элементов в стурктуре

    public DynamicArray(){
        array = new Object[6];
    };

    public DynamicArray(int initialCapacity) {
        if (initialCapacity <= 0) {
            initialCapacity = 10;
        }
        array = new Object[initialCapacity];
    }


    @Override
    public void add(Object value) {
        if (array.length == size) {
            Object[] oldArray = array;
            array = new Object[(int) (size * 1.5d)];
            System.arraycopy(oldArray, 0, array, 0, size); // и с динамическими сработает.
        }
        array[size++] = value;
    }


    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    @Override
    public int size() {
        return size;
    }


    @Override
    public void remove(int index) {
        System.arraycopy(array, index + 1, array, index, (array.length - index - 1));
        size--;
    }


    @Override
    public void set(int index, Object value) {
        array[index] = value;
    }


    @Override
    public Object get(int index) {
        return array[index];
    }


    @Override
    public String toString() {
        return "DynamicArray " + Arrays.toString(array);
    }

    @Override
    public Iterator iterator() {
        return new Itr();
    }

    private class Itr implements Iterator{
        int cursor;

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public Object next() {
            if (cursor >= size){
                throw new NoSuchElementException();
            } else {
                return array[cursor++];
            }
        }
    }
}
