package Structure;

import Structure.exceptions.EmptyQueueException;
import Structure.exceptions.EmptyStackException;
import Structure.exceptions.QueueOverFlowException;
import Structure.exceptions.StackOverFlowException;

import java.util.*;

public class App {
    public static void main(String[] args) {

////        //Реализация Стека
        System.out.println("Реализация Стека");
        Stack stack = new Stack(6);
        stack.push("abc");
        stack.push(55);
        stack.push("def");
        System.out.println("Стек после 3х операций добавления: " + stack + "\n" +
                "Размер стека: "+ stack.size());
        System.out.println("Операция pop: " + stack.pop());
        System.out.println("Операция peek: " + stack.peek());
        System.out.println("Размер стека: " + stack.size() + "\n");

        // Реализация обработки исключений в стеке
//        StaticStack stStack = new StaticStack();
        // Вариант переполнения стека
//        try {
//            stStack.push("nu-nu");
//            stStack.push("na-na");
//            stStack.push("ny-ny");
//            stStack.push("ni-ni");
//            stStack.push("no-no");
//            stStack.push("ne-ne");
//        }
//        catch (StackOverFlowException exc){
//            exc.printStackTrace();
//        }
//        finally {
//        }
//        System.out.println("Стек переполнен! Необходимо удалить как минимум один элемент");

        // Вариант удаления элемента из пустого стека.
//        try {
//            stStack.pop();
//        }
//        catch (EmptyStackException exc){
//            exc.printStackTrace();
//        }
//        finally {
//            System.out.println("Стек пуст! Необходимо добавить как минимум один элемент");
//        }

//

////        // Реализация очереди
        System.out.println("Реализация очереди");
        AbstractQueue<Object> queue = new Queue();
        queue.add("abc");
        queue.add("def");
        queue.add(55);
        System.out.println("Очередь после 3х операций: "+ queue);
        queue.remove();
        System.out.println("Очередь после 1го удаления: " + queue);
        System.out.println("Операция poll: " + queue.poll());
        System.out.println("Результат метода isEmpty() " + queue.isEmpty());
        System.out.println("Размер очереди: " + queue.size()+ "\n");

//        // Реализация обработки исключений в очереди
//        try{
//            queue.add("def");
//            queue.add("ghj");
//            queue.add("klm");
//            queue.add("npo");
//        }
//        catch (QueueOverFlowException exc){
//            exc.printStackTrace();
//            System.out.println("Очередь переволнена");
//        }

//        try {
//            queue.remove();
//            queue.remove();
//            queue.remove();
//        }
//        catch (EmptyQueueException exc){
//            exc.printStackTrace();
//            System.out.println("Очередь пуста. Нужно добавить как минимум один элемент");
//        }


        // Реализация связного списка или динамического массива (разкоментить что надо использовать)
        System.out.println("Реализация связного списка");
        AbstractStructure<Object> array = new List();
//        AbstractStructure<Object> array = new DynamicArray(6);

        array.add("3");
        array.add(7);
        array.add("34");
        array.add("15");
        array.add(new GregorianCalendar(2019, Calendar.MARCH, 25).getTime());
        array.add(78);

        System.out.println("Демострация работы итератора: ");
        for (Object o : array){
            System.out.println(o);
        }
        System.out.println("\nДемострация работы методов списка и динамического массива: ");
        System.out.println("Первоначальный список: \n" + array + "\n");
        array.remove(1);
        System.out.println("Список без второго элемента: \n" + array  + "\n");
        System.out.println("Четвертый элемент: \n" + array.get(3) + "\n");
        array.set(4, "100");
        System.out.println("Список с переписанным на '100' 5-м элементов: \n" + array  + "\n");
    }
}
