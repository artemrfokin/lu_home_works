package sorting;

public class MyMergeSorter {
    public static int[] mergeSortMy(int[] array1, int[] array2){
        int[] resArray = new int[(array1.length + array2.length)];
        int c1 = 0; // курсор 1го подмассива
        int c2 = 0; // курсор 2ого подмассива
        ascendSort(array1);
        ascendSort(array2);

        int i = 0;
        int j = 0;

        while (i < array1.length && j < array2.length){
            if (array1[i] < array2[j]){
                resArray[i + j] = array1[i];
                i++;
            } else {
                resArray[i + j] = array2[j];
                j++;
            }
        }

        while (i < array1.length){
            resArray[i + j] = array1[i];
            i++;
        }

        while (j < array2.length){
            resArray[i + j] = array2[j];
            j++;
        }
        return resArray;
    }


    private static void ascendSort(int[] array){
        if (array[0] > array[1]){
            int x = array[1];
            array[1] = array[0];
            array[0] = x;
        }
    }

}
