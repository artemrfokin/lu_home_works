package sorting;

import sorting.exceptions.NullLegthException;

import java.util.Comparator;

public class MergeSort {
    public static void sort(Comparable[] a){
        if (a.length == 0){
            throw new NullLegthException();
        }
        Object[] src = new Object[a.length];
        System.arraycopy(a, 0, src, 0, a.length);
        mergeSortAlg(src, a, 0, a.length, 0);
    }

    /*
     * Порядок работы алгоритма:
     *  1) дробим scr на маленькие части и сортируем каждую часть пузырьковым методом;
     *  2) каждые две половины, что обрабатываются внутри каждой рекурсивной функции сравниваем на предмет
     *  их последоватености. Может они уже по порядку стоят;
     *  3) сливаем в dest две отсортированные половины;
     * */
    private static void mergeSortAlg(Object[] src, Object[] dest,
                                     int low, int high, int off){
        int length = high - low;

        // Bubble ascending sorting for small arrays (lower then 7)
        if (length < 7){
            for (int i = low; i < high; i++){
                for (int j = i; j > low && ((Comparable) dest[j-1]).compareTo(dest[j]) > 0; j--){
                    swap(dest, j, j - 1);
                }
            }
            return;
        }

        // Recurse sorting of halves
        int destLow = low;
        int destHigh = high;
        low += off;
        high += off;
        int mid = (low + high) >>> 1;
        mergeSortAlg(dest, src, low, mid, -off);
        mergeSortAlg(dest, src, mid, high, -off);

        // Check for nearly ordered lists
        if (((Comparable) src[mid - 1]).compareTo(src[mid]) <= 0){
            System.arraycopy(src, low, dest, destLow, length);
            return;
        }

        // Merge of halves
        // q >= high || p < mid : check for offset using cases
        for (int i = destLow, p=low, q=mid; i < destHigh; i++){
            if (q >= high || p < mid && ((Comparable)src[p]).compareTo(src[q]) <= 0){
                dest[i] = src[p++];
            } else {
                dest[i] = src[q++];
            }
        }
    }


    public static void sort(Object[] a, Comparator c){
        if (a.length == 0){
            throw new NullLegthException();
        }
        Object[] src = new Object[a.length];
        System.arraycopy(a, 0, src, 0, a.length);
        mergeSortAlg(src, a, 0, a.length, 0, c);
    }


    private static void mergeSortAlg(Object[] src, Object[] dest,
                                     int low, int high, int off,
                                     Comparator c){
        int length = high - low;

        // Bubble ascending sorting for small arrays (lower then 7)
        if (length < 7){
            for (int i = low; i < high; i++){
                for (int j = i; j > low && c.compare(dest[j-1], dest[j]) > 0; j--){
                    swap(dest, j, j - 1);
                }
            }
            return;
        }

        // Recurse sorting of halves
        int destLow = low;
        int destHigh = high;
        low += off;
        high += off;
        int mid = (low + high) >>> 1;
        mergeSortAlg(dest, src, low, mid, -off, c);
        mergeSortAlg(dest, src, mid, high, -off, c);

        // Check for nearly ordered lists
        if (c.compare(src[mid - 1], src[mid]) <= 0){
            System.arraycopy(src, low, dest, destLow, length);
            return;
        }

        // Merge of halves
        // q >= high || p < mid : check for offset using cases
        for (int i = destLow, p=low, q=mid; i < destHigh; i++){
            if (q >= high || p < mid && c.compare(src[p], src[q]) <= 0){
                dest[i] = src[p++];
            } else {
                dest[i] = src[q++];
            }
        }
    }


    private static void swap(Object[] x, int a, int b) {
        Object t = x[a];
        x[a] = x[b];
        x[b] = t;
    }


}
