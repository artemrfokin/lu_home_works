package sorting;

import sorting.cats.Cat;
import sorting.cats.CatAgeComparator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
        // Реализация методов сортировки на целых числах.
        Integer[] integerArray = {3, 45, 1, 544, 235, 2, 77, 54, 65, 5, 101, 4, 99, 31, 8, 7};

//        // Реализация выкидывания исключения на передачу массива нулевой длинны.
//        Integer[] a = new Integer[0];
//        BubbleSorter.sort(a);

        BubbleSorter.sort(integerArray);
        System.out.println("Рузультат сортировки пузырьком: " + Arrays.toString(integerArray));

//        // Реализация выкидывания исключения на передачу массива нулевой длинны.
//        Integer[] a = new Integer[0];
//        MergeSort.sort(a);

        MergeSort.sort(integerArray);
        System.out.println("Результат сортировки слиянием: " + Arrays.toString(integerArray));


        // Реализация методов сортировки на объектах
        List<Cat> cats = new ArrayList<>();
        cats.add(new Cat("Ivan", 10));
        cats.add(new Cat("I", 5));
        cats.add(new Cat("Java", 7));
        cats.add(new Cat("Qwerty", 12));
        cats.add(new Cat("Vaskya", 3));
        cats.add(new Cat("R", 4));
        Cat[] catsArray = cats.toArray(new Cat[0]);

        // Сортировка по именам котов (по умолчанию)
        MergeSort.sort(catsArray);
        System.out.println("Сортировка по именам (по умолчанию):\n" + Arrays.toString(catsArray));
        // Сортировка по возрасту котов с компаратором
        MergeSort.sort(catsArray, new CatAgeComparator());
        System.out.println("Сортировка по возрасту котов:\n" + Arrays.toString(catsArray));
    }
}
