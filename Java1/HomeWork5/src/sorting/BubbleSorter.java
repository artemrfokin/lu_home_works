package sorting;

import sorting.exceptions.NullLegthException;

import java.util.*;

public class BubbleSorter {

    public static void sort(Comparable[] array){
        if (array.length == 0){
            throw new NullLegthException();
        }
        for (int i = 0; i < array.length; i++){
            for (int j = i; j > 0 && array[j-1].compareTo(array[j])> 0; j--){
                Comparable x = array[j];
                array[j] = array[j -1];
                array[j - 1] = x;
            }
        }
    }

    public static void sort(Object[] array, Comparator c){
        if (array.length == 0){
            throw new NullLegthException();
        }
        for (int i = 0; i < array.length; i++){
            for (int j = i; j > 0 && c.compare(array[j-1], array[j])> 0; j--){
                Object x = array[j];
                array[j] = array[j -1];
                array[j - 1] = x;
            }
        }
    }


    // Вначале сам написал, потом увидел реализацию в MergeSort и ее оставил как основную.
    // На всякий случай, оставил алгоритм, который сам написал.
    public static int[] sortMy(int[] array) {
        boolean wereChanges = true;
        while (wereChanges) {
            wereChanges = false;
            for (int i = 0; i < array.length - 1; i++) {
                int x;
                if (array[i] < array[i + 1]) {
                    x = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = x;
                    wereChanges = true;
                }
            }
        }
        Integer[] arrayOfIntegers = Arrays.stream(array).boxed().toArray(Integer[]::new);
        List<Integer> arrayList = new ArrayList<>(Arrays.asList(arrayOfIntegers));
        Collections.reverse(arrayList);
        return arrayList.stream().mapToInt(i->i).toArray();
    }

}
