import static java.lang.StrictMath.*;

import java.lang.StringBuilder;
import java.util.*;

public class NumberSystems {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        // Функция перевода шестнадцатиричного числа в двоичное
        hex2bin(sc);

        // Функция перевода десятичного числа в двоичное
//        dec2bin(sc);

        // Функция перевода двоичного числа в десятичное
//        bin2dec(sc);

        // Функция перевода двоичного числа в шестнадцатиричное
//        bin2hex(sc);

    }


    static int bin2decAlgorithm(String binFigure){
        int len = binFigure.length();
        int sum = 0;

        for (int rate = len - 1; rate >= 0; rate--){
            int index = len - (rate + 1);
            int currentNumber = Integer.parseInt(binFigure.substring(index, index + 1));
            if (currentNumber == 1){
                sum += pow(2.0, (double)rate);
            }
        }
        return sum;
    }


    static String dec2binAlgorithm(int intFigure){
        ArrayList binFigure = new ArrayList();
        int res = 0;
        int odd = 0;

        if (intFigure < 2){
            if (intFigure == 0){
                binFigure.add("0");
            } else if (intFigure == 1){
                binFigure.add("1");
            }
        } else {
            int numerator = intFigure;
            while (res != 1){
                res = numerator / 2;
                odd = numerator % 2;
                numerator = res;
                binFigure.add(Integer.toString(odd));
                }
            binFigure.add(Integer.toString(res));
        }
        Collections.reverse(binFigure);
        return String.join("", binFigure);
    }


    static String dec2hexAlg(String figure) {
        if (Integer.parseInt(figure) > 10) {
            Map<String, String> hexDict = new HashMap<String, String>();
            hexDict.put("10", "A");
            hexDict.put("11", "B");
            hexDict.put("12", "C");
            hexDict.put("13", "D");
            hexDict.put("14", "E");
            hexDict.put("15", "F");
            return hexDict.get(figure);
        }
            return figure;
    }


    static int hex2dec(String hexFigure){
        if (hexFigure.matches("\\d")){
            return Integer.parseInt(hexFigure);
        } else {
            Map<String, Integer> hexDict = new HashMap<String, Integer>();
            hexDict.put("A", 10);
            hexDict.put("B", 11);
            hexDict.put("C", 12);
            hexDict.put("D", 13);
            hexDict.put("E", 14);
            hexDict.put("F", 15);
            return hexDict.get(hexFigure);
        }
    }


    static void bin2dec(Scanner sc){
        System.out.print("Пожалуйста, введите двоичное число: ");
        String binFigure = sc.nextLine();
        int sum = bin2decAlgorithm(binFigure);
        System.out.printf("В десятичной СС число %s равно %d.", binFigure, sum);
    }


    static void bin2hex(Scanner sc){
        System.out.print("Пожалуйста, введите двоичное число для перевода в 16-ное: ");
        String binFigure = sc.nextLine();

        int len = binFigure.length();
        int groupsQuantity = 0;
        String answer = "";

        if (len == 0){
            System.out.println("Вы не ввели число");
        } else if (len <= 4){
            groupsQuantity = 1;
        } else if (len > 4){
            groupsQuantity = len / 4;
            if (len % 4 != 0){
                groupsQuantity += 1;
            }
        }

        int odd = len % 4;
        boolean isFirstGroupProccesed = false;

        for (int i = 0; i < groupsQuantity; i++) {
            String currGroup = "";

            if (groupsQuantity == 1) {
                currGroup = binFigure;
            } else if (len % 4 == 0) {
                currGroup = binFigure.substring(i * 4, 4 * i + 4);
            } else if (i == 0) {
                currGroup = binFigure.substring(0, odd);
            } else {
                currGroup = binFigure.substring((i - 1) * 4 + odd, 4 * (i - 1) + 4 + odd);
            }

            String currFigure = Integer.toString(bin2decAlgorithm(currGroup));
            currFigure = dec2hexAlg(currFigure);
            answer += currFigure;
        }
        System.out.printf("Число в шестнадцатиричной системе = %s", answer);
    }


    static void dec2bin(Scanner sc) {
        System.out.println("Пожалуйста, введите десятизначное число:");
        int decFigure = Integer.parseInt(sc.nextLine());
        System.out.println(dec2binAlgorithm(decFigure));
    }


    static void hex2bin(Scanner sc){
        System.out.println("Пожалуйста, введите шеснадцатиричное число:");
        String hexFifure = sc.nextLine();
        StringBuilder sb = new StringBuilder();
        for (String letter : hexFifure.split("")){
            int decFigure = hex2dec(letter);
            String binFigure = dec2binAlgorithm(decFigure);
            if (binFigure.length() < 4){
                String[] nulls = new String[4 - binFigure.length()];
                Arrays.fill(nulls, "0");
                String result = String.join("", nulls) + binFigure + " ";
                sb.append(result);
            } else {
                sb.append(binFigure);
                sb.append(" ");
            }
        }
        System.out.println(sb.toString());
    }
}

