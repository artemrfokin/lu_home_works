public class MathOperations {
    public static void main(String[] args){
        int a = 5;
        int b = 4;

        // Целые числа
        int sum_int = a + b;
        int sub_int = a - b;
        int mul_int = a * b;
        int div_int = a / b;
        int odd_int = a % b;
        System.out.println("Целые числа:");
        System.out.println(
                "sum_int = " + sum_int + "\n" +
                "sub_int = " + sub_int + "\n" +
                "mul_int = " + mul_int + "\n" +
                "div_int = " + div_int + "\n" +
                "odd_int = " + odd_int + "\n");

        // С плавающей точкой float
        float sum_f = (float)(a + b);
        float sub_f = (float)(a - b);
        float mul_f = (float)(a * b);
        float div_f = (float)(a / b);
        float odd_f = (float)(a % b);
        System.out.println("С плавающей точкой float:");
        System.out.println(
                "sum_f = " + sum_f + "\n" +
                "sub_f = " + sub_f + "\n" +
                "mul_f = " + mul_f + "\n" +
                "div_f = " + div_f + "\n" +
                "odd_f = " + odd_f + "\n");

        // С плавающей точкой double
        double sum_d = (double)(a + b);
        double sub_d = (double)(a - b);
        double mul_d = (double)(a * b);
        double div_d = (double)(a / b);
        double odd_d = (double)(a % b);
        System.out.println("С плавающей точкой double:");
        System.out.println(
                "sum_d = " + sum_d + "\n" +
                "sub_d = " + sub_d + "\n" +
                "mul_d = " + mul_d + "\n" +
                "div_d = " + div_d + "\n" +
                "odd_d = " + odd_d + "\n");

        System.out.println("Отличий в результатах нет. Различие только в представлении чисел. " +
                "Но! при прведении чисел из int в float, числа более 60 млн.теряют точность.");
    }
}
