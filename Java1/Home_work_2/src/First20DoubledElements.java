public class First20DoubledElements {
    public static void main(String[] args) {
        int n = 0;
        for (int i = 2; n <= 20; i*=2){
            System.out.printf("%,d, ", i);
            n+=1;
        }
    }
}
