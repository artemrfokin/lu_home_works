import java.util.Scanner;
import java.lang.Math;

public class isSimpleFigure {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("Пожалуйста, введите число: ");
            isInputDataCorrect(sc);
            int n = sc.nextInt();

            int k = 0;
            if (n > 3) {
                double sqRoot = 0.0;
                for (int i = 2; i <= n; i++) {

                    if (i <= 3) {
                        k = i;
                    } else if (i % 2 != 0 || i % 3 != 0 || i <= Math.pow((double)(n), 0.5) || i == n) {
                        k = i;
                    } else {
                        continue;
                    }

                    if (n % k == 0 && k != n) {
                        System.out.printf("Число %d составное. У него есть делитель %d. \n \n", n, k);
                        break;
                    } else if (k == n) {
                        System.out.printf("Число %d простое.\n \n", n);
                    }
                }
            } else {
                System.out.printf("Число %d простое. \n\n", n);
            }
        }
    }

    static void isInputDataCorrect(Scanner sc) {
        while (!sc.hasNextInt()) {
            System.out.println("Вы ввели не число. Пожалуйста, введите число.\n");
            sc.next();
        }
    }
}

