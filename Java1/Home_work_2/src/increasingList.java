import java.util.Scanner;

public class increasingList {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] diglist = new int[3];

        System.out.println("Пожалуйста, введите целое число a.");
        isInputDataCorrect(sc);
        int a = sc.nextInt();
        diglist[0] = a;
        System.out.println("Пожалуйста, введите целое число b.");
        isInputDataCorrect(sc);
        int b = sc.nextInt();
        diglist[1] = b;
        System.out.println("Пожалуйста, введите целое число с.");
        isInputDataCorrect(sc);
        int c = sc.nextInt();
        diglist[2] = c;

        int max = 0;
        int min = 0;

        for (int i = 0; i < 3; i++){
            if (i == 0){
                max = diglist[i];
                min = diglist[i];
            } else {
                if (diglist[i] >= max){
                    max = diglist[i];
                } else {
                    min = diglist[i];
                }
            }
        }

        int[] resultList = new int[3];
        resultList[0] = min;
        resultList[2] = max;

        for (int i = 0; i < 3; i++){
            if (diglist[i] > min && diglist[i] < max){
                resultList[1] = diglist[i];
            }
        }
        System.out.printf("Числа в переменных a, b и c: %d, %d, %d \n", a, b, c);
        System.out.printf("Возрастающая последовательность: %d, %d, %d",
                resultList[0], resultList[1], resultList[2]);

    }

    static void isInputDataCorrect(Scanner sc){
        while (!sc.hasNextInt()){
            System.out.println("Вы ввели не число. Пожалуйста, введите число.");
            sc.next();
        }
    }
}
