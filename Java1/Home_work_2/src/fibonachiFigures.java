import java.util.ArrayList;
import java.util.Scanner;

public class fibonachiFigures {
    public static void main(String[] args) {
        int n = 11;
        ArrayList list = new ArrayList();

        for (int i = 0; i < n; i++){
            if ( i <= 1){
                list.add(i,1);
            } else {
                list.add(i, ((int)(list.get(i - 2)) + (int)(list.get(i - 1))));
            }
        }
        System.out.println(String.join(" ", list.toString()));
    }
}
