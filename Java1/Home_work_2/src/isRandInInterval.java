import java.util.Random;

public class isRandInInterval {
    public static void main(String[] args) {
        Random rnd = new Random();
        int randFigure = rnd.nextInt(150) + 5;

        if (randFigure > 25 && randFigure < 100){
            System.out.printf("Число %d содержится в интервале (25,100).", randFigure);
        } else {
            System.out.printf("Число %d не содержится в интервале (25,100).", randFigure);
        }
    }
}
