import java.util.Random;

public class TreeSymbolDigitAndTheBiggestOne {
    public static void main(String[] args) {
        Random rnd = new Random();
        int rndFigure = rnd.nextInt(899) + 100;

        int[] digitsList = new int[3];
        digitsList[0] = rndFigure / 100;
        digitsList[1] = (rndFigure - digitsList[0] * 100) / 10;
        digitsList[2] = (rndFigure % 10);

        int max = 0;
        for (int i = 0; i < 3; i++){
            if (digitsList[i] > max){
                max = digitsList[i];
            }
        }
        System.out.printf("В числе %d наибольшая цифра %d.", rndFigure, max);
    }
}
