import java.util.Scanner;

public class isEven {
    public static void main(String[] args){
        System.out.print("Пожалуйста, введите целове число: ");
        Scanner sc = new Scanner(System.in);

        while (!sc.hasNextInt()){
            System.out.print("Пожалуйста, введите целове число: ");
            sc.next();
        }

        int n = sc.nextInt();
        if (n % 2 == 0){
            System.out.printf("Число %d является четным.", n);
        } else {
            System.out.printf("Число %d является нечетным.", n);
        }
    }
}
