import java.util.ArrayList;
import java.util.Scanner;

public class PositiveDividers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Пожалуйста, введите число: ");
        isInputDataCorrect(sc);
        int a = sc.nextInt();

        ArrayList list = new ArrayList();
        for (int i = 1; i <= a; i++){
            if (a % i == 0){
                list.add(Integer.toString(i));
            }
        }

        System.out.print("Список множителей: " + String.join(" ", list));
    }


    static void isInputDataCorrect(Scanner sc){
        while (!sc.hasNextInt()){
            System.out.println("Вы ввели не число. Пожалуйста, введите число.");
            sc.next();
        }
    }
}
