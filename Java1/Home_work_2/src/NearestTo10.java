import java.util.Scanner;
import java.lang.Math;

public class NearestTo10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Пожалуйста, введите число m.");
        isInputDataCorrect(sc);
        double m = sc.nextDouble();

        System.out.println("Пожалуйста, введите число n.");
        isInputDataCorrect(sc);
        double n = sc.nextDouble();

        if (Math.abs(m - 10) < Math.abs(n - 10)){
            System.out.printf("Число %.2f ближе к 10 чем число %.2f.", m, n);
        } else {
            System.out.printf("Число %.2f ближе к 10 чем число %.2f.", n, m);
        }
    }

    static void isInputDataCorrect(Scanner sc){
        while (!sc.hasNextDouble()){
            System.out.println("Вы ввели не число. Пожалуйста, введите число.");
            sc.next();
        }
    }
}
