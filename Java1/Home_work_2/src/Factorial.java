import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Пожалуйста, введите число факториал которого хотите узнать: ");
        isInputDataCorrect(sc);
        int a = sc.nextInt();
        int factorial = 1;

        if (a == 0){
            factorial = 0;
        } else {
            for (int i = 1; i <= a; i++){
                factorial *= i;
            }
        }
        System.out.printf("Факториал числа %d = %,d.", a, factorial);

    }
    static void isInputDataCorrect(Scanner sc){
        while (!sc.hasNextInt()){
            System.out.println("Вы ввели не число. Пожалуйста, введите число.");
            sc.next();
        }
    }
}
