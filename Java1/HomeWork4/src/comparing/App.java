package comparing;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        Car c1 = new Car("Toyota", "Land Cruser", "Petrol",
                "Robot", "2019");
        Car c2 = new Car("Toyota", "Highlander", "Petrol",
                "Robot", "2018");
        Car c3 = new Car("Toyota", "Pilot", "Diesel",
                "Auto", "2017");
        Car c4 = new Car("Toyota", "Land Cruser", "Petrol",
                "Auto", "2019");
        Car c5 = new Car("Toyota", "Highlander", "Diesel",
                "Robot", "2019");
        Car c6 = new Car("Toyota", "Pilot", "Diesel",
                "Robot", "2019");

        Car wishCar = new Car("", "Highlander", "Petrol",
                "Robot", "");

        Car[] carList = {c1, c2, c3, c4, c5, c6};
        for (Object car : carList) {
            System.out.println(car.equals(wishCar));
            System.out.println(c1);
        }
    }
}

