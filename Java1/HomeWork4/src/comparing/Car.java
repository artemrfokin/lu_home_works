package comparing;

import java.util.Objects;

public class Car {
    String manufacturer;
    String model;
    String engineType;
    String gearType;
    String year;

    Car (String manufacturer, String model, String engineType, String gearType, String year){
        this.manufacturer = manufacturer;
        this.model = model;
        this.engineType = engineType;
        this.gearType = gearType;
        this.year = year;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Car)) return false;

        Car other = (Car) obj;
        return Objects.equals(this.model, other.model) &&
                Objects.equals(this.engineType, other.engineType) &&
                Objects.equals(this.gearType, other.gearType);
    }

    @Override
    public int hashCode() {
        int result = 23;
        result += result * 5 + this.model.hashCode();
        result += result * 31 + this.engineType.hashCode();
        result += result * 11 + this.gearType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return getClass() + ": manufacturer - " + manufacturer + ", model - " + model;
    }
}
